#include <CUnit/CUnit.h>

extern CU_TestInfo tests_maxmod[];

void test_maxmod_constant(void);
void test_maxmod_linear(void);
void test_maxmod_unit_coefficient(void);
void test_maxmod_plateau(void);
void test_maxmod_small_perturbation(void);
void test_maxmod_monomial_short_circuit(void);
void test_maxmod_simplified(void);
void test_maxmod_rudin_shapiro(void);
