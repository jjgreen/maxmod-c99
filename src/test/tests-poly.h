#include <CUnit/CUnit.h>

extern CU_TestInfo tests_poly[];

void test_poly_mult(void);
void test_poly_conj(void);
void test_poly_eval(void);
void test_poly_sqr_abs_eval(void);
void test_poly_l1norm(void);
