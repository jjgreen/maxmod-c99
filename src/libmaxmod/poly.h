#ifndef POLY_H
#define POLY_H

#include <stddef.h>
#include <complex.h>

int poly_mult(const double complex*, size_t,
              const double complex*, size_t,
              double complex*);
int poly_conj(const double complex*, size_t, double complex*);
double complex poly_eval(const double complex*, size_t, double complex);
double poly_sqr_abs_eval(const double complex*, size_t, double complex);
double poly_l1norm(const double complex*, size_t);

#endif
