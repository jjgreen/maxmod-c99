#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "poly.h"

int poly_mult(const double complex *p1, size_t n1,
              const double complex *p2, size_t n2,
              double complex *p)
{
  if (n1 > n2)
    return poly_mult(p2, n2, p1, n1, p);

  for (size_t i = 0 ; i <= n1 ; i++)
    {
      double complex sum = 0;

      for (size_t j = 0 ; j <= i ; j++)
	sum += p1[j] * p2[i - j];

      p[i] = sum;
    }

  if (n2 > n1)
    {
      for (size_t i = n1 + 1 ; i <= n2 ; i++)
	{
	  double complex sum = 0;

	  for (size_t j = 0 ; j <= n1 ; j++)
	    sum += p1[j] * p2[i - j];

	  p[i] = sum;
	}
    }

  for (size_t i = n2 + 1 ; i <= n1 + n2 ; i++)
    {
      double complex sum = 0.0;

      for (size_t j = i - n2 ; j <= n1 ; j++)
	sum += p1[j] * p2[i - j];

      p[i] = sum;
    }

  return 0;
}

int poly_conj(const double complex *p, size_t n, double complex *q)
{
  for (size_t i = 0 ; i <= n ; i++)
    q[n - i] = conj(p[i]);

  return 0;
}

double complex poly_eval(const double complex *p, size_t n, double complex z)
{
  double complex val;

  for (p += n, val = *p ; n-- ; val = val * z + *--p);

  return val;
}

double poly_sqr_abs_eval(const double complex *p, size_t n, double complex z)
{
  double complex
    val = poly_eval(p, n, z);
  double
    re = creal(val),
    im = cimag(val);

  return re * re + im * im;
}

double poly_l1norm(const double complex *c, size_t n)
{
  double sum = 0, e = 0;

  for (size_t i = 0 ; i <= n ; i++)
    {
      double
	tmp = sum,
	y = cabs(c[i]) + e;

      sum = tmp + y;

      e = (tmp - sum) + y;
    }

  return sum;
}
