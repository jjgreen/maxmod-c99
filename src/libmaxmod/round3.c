#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "round3.h"

size_t round3(size_t n)
{
  size_t m;

  for (m = 3 ; m < n ; m *= 3);

  return m;
}
