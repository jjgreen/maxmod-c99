#ifndef IVTABLE_H
#define IVTABLE_H

#include <stddef.h>
#include <complex.h>

typedef struct ivtable_t ivtable_t;

ivtable_t* ivtable_new(void);
void ivtable_destroy(ivtable_t*);

size_t ivtable_evaluated(const ivtable_t*);
size_t ivtable_total(const ivtable_t*);
size_t ivtable_active(const ivtable_t*);
long ivtable_width(const ivtable_t*);
double ivtable_delta(const ivtable_t*);

int ivtable_init(size_t, size_t, ivtable_t*);
int ivtable_eval(const double complex*, size_t, ivtable_t*, size_t);
double ivtable_largest(const ivtable_t*);
int ivtable_reject(ivtable_t*, double, size_t*);
int ivtable_subdivide(ivtable_t*, size_t);

#endif
