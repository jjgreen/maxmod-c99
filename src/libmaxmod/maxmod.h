/*
  maxmod.h

  calulate the maximum modulus of a complex polynomial
  on the unit disc; see maxmod(3) for details

  J.J. Green
*/

#ifndef MAXMOD_H
#define MAXMOD_H

#include <stddef.h>
#include <complex.h>

#define MAXMOD_OK 0
#define MAXMOD_INACCURATE 1
#define MAXMOD_MALLOC 2
#define MAXMOD_POLY 3

typedef struct
{
  double relerror;
  struct
  {
    size_t intervals, evaluations;
  } max;
} maxmod_opt_t;

typedef struct
{
  double maxmod;
  double relerror;
  double width;
  size_t evaluations;
} maxmod_results_t;

int maxmod_d(const double complex*, size_t, maxmod_opt_t, maxmod_results_t*);
double maxmod(const double complex*, size_t);

#endif
